import 'package:firebase_auth/firebase_auth.dart';

class AuthFirebase {
  FirebaseAuth auth = FirebaseAuth.instance;

  Future<String> signIn(String email, String password) async {
    UserCredential userCredential =
        await auth.signInWithEmailAndPassword(email: email, password: password);
    return userCredential.user.uid;
  }

  Future<String> createUser(String email, String password) async {
    UserCredential userCredential = await auth.createUserWithEmailAndPassword(
        email: email, password: password);
    return userCredential.user.uid;
  }

  Future<String> currentUser() async {
    //UserCredential userCredential = await auth.currentUser;
    if (auth.currentUser != null) {
      return auth.currentUser.uid;
    } else
      return null;
  }

  Future<void> signOut() async {
    return auth.signOut();
  }
}
