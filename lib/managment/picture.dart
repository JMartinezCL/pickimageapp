import 'package:firebase_database/firebase_database.dart';

class Picture {
  String key, name, description, image;

  Picture(this.key, this.name, this.description, this.image);

  Picture.getPicture(DataSnapshot snap)
      : key = snap.key,
        name = snap.value["name"],
        description = snap.value["description"],
        image = snap.value["image"];
}
