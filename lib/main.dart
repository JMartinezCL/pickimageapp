import 'package:ff_app/managment/authFirebase.dart';
import 'package:ff_app/screens/rootPage.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:ff_app/screens/loginPage.dart';
import 'package:flutter/material.dart';
import 'package:loading/loading.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';

void main() {
  runApp(App());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: LoginPage(title: "Firebase Test"),
    );
  }
}

class App extends StatefulWidget {
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  // Set default `_initialized` and `_error` state to false
  bool _initialized = false;
  bool _error = false;

  // Define an async function to initialize FlutterFire
  void initializeFlutterFire() async {
    try {
      // Wait for Firebase to initialize and set `_initialized` state to true
      await Firebase.initializeApp();
      setState(() {
        _initialized = true;
        debugPrint("Initialized! :)");
      });
    } catch (e) {
      // Set `_error` state to true if Firebase initialization fails
      setState(() {
        _error = true;
      });
    }
  }

  @override
  void initState() {
    initializeFlutterFire();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // Show error message if initialization failed
    if (_error) {
      return Text("wrong!");
    }

    // Show a loader until FlutterFire is initialized
    if (!_initialized) {
      return MaterialApp(
          home: Scaffold(
        body: Center(
          child: Loading(indicator: BallPulseIndicator(), size: 100.0),
        ),
      ));
    }

    return MaterialApp(
      title: 'Flutter Demo',
      home: RootPage(
        authFirebase: new AuthFirebase(),
      ),
    );
  }
}
