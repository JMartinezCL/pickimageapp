import 'package:ff_app/managment/authFirebase.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title, this.auth, this.onSignIn}) : super(key: key);
  final String title;
  final AuthFirebase auth;
  final VoidCallback onSignIn;

  @override
  _LoginPageState createState() => _LoginPageState();
}

enum FormType { login, register }

class _LoginPageState extends State<LoginPage> {
  final formKey = new GlobalKey<FormState>();
  FormType formType = FormType.login;
  var email = TextEditingController();
  var password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          title: Text(widget.title),
        ),
        backgroundColor: Colors.purple[50],
        body: SingleChildScrollView(
          padding: EdgeInsets.all(16.0),
          child: new Form(
            key: formKey,
            child: new Column(
              children: formLogin(),
            ),
          ),
        ));
  }

  List<Widget> formLogin() {
    return [
      padded(
          child: TextFormField(
        controller: email,
        decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: "Email",
        ),
        autocorrect: false,
      )),
      padded(
          child: TextFormField(
        controller: password,
        decoration: InputDecoration(
          icon: Icon(Icons.lock),
          labelText: "Password",
        ),
        autocorrect: false,
        obscureText: true,
      )),
      new Column(children: buttonWidget())
    ];
  }

  List<Widget> buttonWidget() {
    switch (formType) {
      case FormType.login:
        return [
          styleButton("Sign in", validateSession),
          new FlatButton(
            child: new Text("Do you dont have an account? Sign up"),
            onPressed: () => updateFormType(FormType.register),
          )
        ];
      case FormType.register:
        return [
          styleButton("Sign up", validateSession),
          new FlatButton(
            child: new Text("Start Session"),
            onPressed: () => updateFormType(FormType.login),
          )
        ];
      default:
        return [];
    }
  }

  void updateFormType(FormType form) {
    formKey.currentState.reset();
    setState(() {
      formType = form;
    });
  }

  void validateSession() {
    (formType == FormType.login)
        ? widget.auth.signIn(email.text, password.text)
        : widget.auth.createUser(email.text, password.text);

    widget.onSignIn();
  }

  Widget styleButton(String text, VoidCallback onPressed) {
    return new RaisedButton(
      onPressed: onPressed,
      color: Colors.blue,
      child: new Text(
        text,
        style: TextStyle(color: Colors.white),
      ),
    );
  }

  Widget padded({Widget child}) {
    return new Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0),
      child: child,
    );
  }
}
