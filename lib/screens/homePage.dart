import 'package:ff_app/Widgets/pictureListview.dart';
import 'package:ff_app/managment/authFirebase.dart';
import 'package:ff_app/screens/picturePage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class HomePage extends StatelessWidget {
  HomePage({this.onSignIn, this.authFirebase});
  final VoidCallback onSignIn;
  final AuthFirebase authFirebase;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        actions: [
          new FlatButton(
              onPressed: signOut,
              child: Text("Logout", style: TextStyle(color: Colors.white)))
        ],
        title: Text("Home"),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => FormPicture("New Picture", null)));
        },
        shape: StadiumBorder(),
        backgroundColor: Colors.blue,
        child: Icon(
          Icons.add,
          size: 20.0,
        ),
      ),
      body: new PictureListView(context),
    );
  }

  void signOut() {
    authFirebase.signOut();
    onSignIn();
  }
}
