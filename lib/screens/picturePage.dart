import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';

import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:ff_app/managment/picture.dart';

class FormPicture extends StatefulWidget {
  final String title;
  final Picture picture;
  FormPicture(this.title, this.picture);
  @override
  State<StatefulWidget> createState() => new FormPictureState();
}

class FormPictureState extends State<FormPicture> {
  var nameController = TextEditingController();
  var descriptionController = TextEditingController();
  File _image;
  final picker = ImagePicker();
  String urlPicture;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: new Text(widget.title),
      ),
      body: new SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: getFormPicture(),
      ),
    );
  }

  @override
  void initState() {
    if (widget.picture != null) {
      nameController.text = widget.picture.name;
      descriptionController.text = widget.picture.description;
    }
    super.initState();
  }

  Widget getFormPicture() {
    return new Column(
      children: [
        new TextFormField(
          decoration: InputDecoration(
              icon: Icon(Icons.image),
              hintText: "What's Name",
              labelText: "Name"),
          controller: nameController,
        ),
        new TextFormField(
          decoration: InputDecoration(
              icon: Icon(Icons.description),
              hintText: "Some description",
              labelText: "Description"),
          controller: descriptionController,
        ),
        new RaisedButton(
            onPressed: pictureSelector, child: Text("Select an image")),
        new SizedBox(
            child: _image == null ? Text('No image selected.') : showPicture()),
        new RaisedButton(
          onPressed: sendData,
          child: Text("Save"),
        )
      ],
    );
  }

  sendData() {
    saveFirebase(nameController.text).then((_) {
      DatabaseReference db =
          FirebaseDatabase.instance.reference().child("Picture");

      if (widget.picture != null) {
        db.child(widget.picture.key).set(getPicture()).then((_) {
          Navigator.pop(context);
        });
      } else {
        db.push().set(getPicture()).then((_) {
          Navigator.pop(context);
        });
      }
    });
  }

  Map<String, dynamic> getPicture() {
    Map<String, dynamic> data = new Map();
    data["name"] = nameController.text;
    data["description"] = descriptionController.text;

    if (widget.picture != null && _image == null) {
    } else {
      data["image"] = (urlPicture != null ? urlPicture : "");
    }
    return data;
  }

  Future<void> saveFirebase(String imageId) async {
    if (_image != null) {
      StorageReference reference =
          FirebaseStorage.instance.ref().child("picture").child(imageId);
      StorageUploadTask uploadTask = reference.putFile(_image);
      StorageTaskSnapshot downloadUrl = (await uploadTask.onComplete);
      urlPicture = (await downloadUrl.ref.getDownloadURL());
    }
  }

  showPicture() {
    if (_image != null) {
      return new Image.file(_image);
    } else {
      if (widget.picture != null) {
        return FadeInImage.assetNetwork(
            placeholder: "images/flutter.png",
            image: widget.picture.image,
            height: 800.0,
            width: 600.0);
      } else {
        return Text("No image selected.");
      }
    }
  }

  Future pictureSelector() async {
    final pickedFile = await picker.getImage(
        source: ImageSource.gallery, maxHeight: 800.0, maxWidth: 700.0);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
        return new Text("No image selected");
      }
    });
  }
}
