import 'dart:async';

import 'package:ff_app/Widgets/pictureCard.dart';
import 'package:ff_app/managment/picture.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';

class PictureListView extends StatefulWidget {
  final BuildContext context;
  PictureListView(this.context);

  @override
  State<StatefulWidget> createState() => new PictureListViewState();
}

class PictureListViewState extends State<PictureListView> {
  DatabaseReference reference =
      FirebaseDatabase.instance.reference().child("Picture");
  StreamSubscription<Event> onAddedSubs;
  StreamSubscription<Event> onChangedSubs;
  List<Picture> pictures = new List();

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      shrinkWrap: true,
      itemCount: pictures.length,
      itemBuilder: (BuildContext context, int index) {
        return new PictureCardView(pictures[index], context);
      },
    );
  }

  void initState() {
    onAddedSubs = reference.onChildAdded.listen(onEntryAdded);
    onChangedSubs = reference.onChildChanged.listen(onEntryChanged);
    super.initState();
  }

  onEntryAdded(Event event) {
    setState(() {
      pictures.add(Picture.getPicture(event.snapshot));
    });
  }

  onEntryChanged(Event event) {
    Picture oldEntry = pictures.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });
    setState(() {
      pictures[pictures.indexOf(oldEntry)] = Picture.getPicture(event.snapshot);
    });
  }

  void dispose() {
    onAddedSubs.cancel();
    onChangedSubs.cancel();
  }
}
