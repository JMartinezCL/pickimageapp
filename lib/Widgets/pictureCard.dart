import 'package:ff_app/managment/picture.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class PictureCardView extends StatelessWidget {
  final Picture picture;
  final BuildContext context;
  PictureCardView(this.picture, this.context);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new Container(
          height: 144.0,
          width: 500.0,
          color: Colors.lightBlue[100],
          child: FadeInImage.assetNetwork(
            placeholder: "images/flutter.png",
            image: picture.image,
            height: 144.0,
            width: 160.0,
          ),
        ),
        new Padding(
          padding: new EdgeInsets.all(7.0),
          child: new Row(
            children: [
              new Padding(
                padding: new EdgeInsets.all(7.0),
                child: new Icon(Icons.image),
              ),
              new Padding(
                padding: new EdgeInsets.all(7.0),
                child: new Text(
                  picture.name,
                  style: TextStyle(color: Colors.blueGrey, fontSize: 18.0),
                ),
              ),
              new Padding(
                padding: new EdgeInsets.all(7.0),
                child: new Text(
                  picture.description,
                  style: TextStyle(color: Colors.blueGrey, fontSize: 14.0),
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
