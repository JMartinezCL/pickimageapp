# PickImageApp

An application for testing firebase (Auth, Database-real-time and Storage) connection on flutter. 
Features on the TodoApp include:
- Create user and login.
- Select image and save it.
- Add name and description to the image.


## Getting Started
- Download zip project or clone git repository on your local workspace.
- Connect mobile device to your computer, or start emulated device.
- Run "flutter doctor -v" to check everything is ok.
- Run "flutter run" or "flutter run -v" for more info.

Used Packages:
- firebase_core
- firebase_analytics
- firebase_database
- firebase_storage
- firebase_auth
- image_picker

## Screenshoots

<p align="center">
  <img src="images/sc01.jpg" width="200" alt="1"/>
  <img src="images/sc02.jpg" width="200" alt="2"/> 
  <img src="images/sc03.jpg" width="200" alt="3"/>
  <img src="images/sc04.jpg" width="200" alt="4"/>
</p>

<p align="center">
  <img src="images/sc05.jpg" width="200" alt="5"/>
  <img src="images/sc06.jpg" width="200" alt="6"/> 
  <img src="images/sc07.jpg" width="200" alt="7"/>
</p>

<p align="center">
  <img src="images/01.gif" width="200" alt="g1"/>
  <img src="images/02.gif" width="200" alt="g1"/> 
</p>

**-Screenshoot get on Android Lollipop-**
# Authors
* **José Martínez** - [JMartinezCL](https://gitlab.com/JMartinezCL)

